#!/bin/bash

DATE=`date +%Y%m%d_%H_%M_%S`

if [ -d "kubernetes" ]; then
   echo "ERROR:: Kubernetes directory exists in the root path. Please verify and restart the installation."
   exit 1
fi

cd ~/templates
if [ ! -f "api_key.json" ]; then
   echo "ERROR:: Googe Cloud api key is not available at ~/templates/api_key.json. Please verify and restart the installation."
   exit 1
fi
cd ~/

#install unzip
sudo apt --assume-yes install unzip

#install python
sudo apt-get update
sudo apt --assume-yes install python3-minimal

#install pip
sudo apt update
sudo apt --assume-yes install python3-pip
#install jq
sudo apt --assume-yes install jq

mkdir -p ~/kubernetes
cd ~/

#Download terraform
if [ ! -d terraform ] ; then
        wget -q https://releases.hashicorp.com/terraform/0.11.11/terraform_0.11.11_linux_amd64.zip
        unzip terraform_0.11.11_linux_amd64.zip -d terraform
fi
cwd=`pwd`
echo $cwd
export PATH=$PATH:$cwd/terraform
echo $PATH
cd ~/kubernetes
git clone https://bitbucket.org/akurathiph/kubespray-gcp-terraform.git ~/kubernetes/kube-terraform
cd ~/kubernetes/kube-terraform/hackathon


cp ~/kubernetes/kube-terraform/templates/terraform.tfvars .
cp ~/templates/api_key.json .

project_id=`gcloud config get-value project 2> /dev/null`
sed -i -e "s/project-id/$project_id/g" terraform.tfvars

#res_prefix=`grep res_prefix terraform.tfvars |tr -d "[:blank:]"|sed 's/#.*$//'|cut -f 1 -d "="`
export $(grep res_prefix terraform.tfvars |tr -d "[:blank:]"|sed 's/#.*$//')
cd ~/kubernetes/kube-terraform/scripts
sh kubespray.sh
cd ~/kubernetes/kube-terraform/hackathon
master_internal=$(gcloud compute instances list --filter="${res_prefix}-masters" --format=json |jq -r '.[].networkInterfaces[].networkIP'|tr "\n" " "| sed -e "s/ \{1,\}$//")
node1_internal=$(gcloud compute instances list --filter="${res_prefix}-workers" --format=json |jq -r '.[].networkInterfaces[].networkIP'|tr "\n" " "| sed -e "s/ \{1,\}$//"| cut -f 1 -d " ")
node2_internal=$(gcloud compute instances list --filter="${res_prefix}-workers" --format=json |jq -r '.[].networkInterfaces[].networkIP'|tr "\n" " "| sed -e "s/ \{1,\}$//"| cut -f 2 -d " ")


master_ip=$(gcloud compute instances list --filter="${res_prefix}-masters" --format=json |jq -r '.[].networkInterfaces[].accessConfigs[].natIP'|tr "\n" " "| sed -e "s/ \{1,\}$//")
node1_ip=$(gcloud compute instances list --filter="${res_prefix}-workers" --format=json |jq -r '.[].networkInterfaces[].accessConfigs[].natIP'|tr "\n" " "| sed -e "s/ \{1,\}$//"| cut -f 1 -d " ")
node2_ip=$(gcloud compute instances list --filter="${res_prefix}-workers" --format=json |jq -r '.[].networkInterfaces[].accessConfigs[].natIP'|tr "\n" " "| sed -e "s/ \{1,\}$//"| cut -f 2 -d " ")

echo "$master_internal:$master_ip:$node1_ip:$node2_ip"
echo "Enabling ssh"
yes y | ssh-keygen -t rsa -f ~/.ssh/id_rsa -N ""
ssh-keygen -f "/home/a_phanimca/.ssh/known_hosts" -R $master_ip
cat ~/.ssh/id_rsa.pub | ssh -o "StrictHostKeyChecking no" -i cust_id_rsa core@$master_ip "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys && chmod 640 ~/.ssh/authorized_keys"
ssh-keygen -f "/home/a_phanimca/.ssh/known_hosts" -R $node1_ip
cat ~/.ssh/id_rsa.pub | ssh -o "StrictHostKeyChecking no" -i cust_id_rsa core@$node1_ip "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys && chmod 640 ~/.ssh/authorized_keys"
ssh-keygen -f "/home/a_phanimca/.ssh/known_hosts" -R $node2_ip
cat ~/.ssh/id_rsa.pub | ssh -o "StrictHostKeyChecking no" -i cust_id_rsa core@$node2_ip "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys && chmod 640 ~/.ssh/authorized_keys"

git clone https://github.com/kubernetes-incubator/kubespray.git
cd kubespray
git checkout tags/v2.8.1


sudo pip3 install -r requirements.txt
cp -rfp inventory/sample inventory/mycluster
cp ~/kubernetes/kube-terraform/templates/hosts.ini inventory/mycluster

sed -i -e "s/master_external/$master_ip/g" inventory/mycluster/hosts.ini
sed -i -e "s/master_internal/$master_internal/g" inventory/mycluster/hosts.ini
sed -i -e "s/node1_external/$node1_ip/g" inventory/mycluster/hosts.ini
sed -i -e "s/node1_internal/$node1_internal/g" inventory/mycluster/hosts.ini
sed -i -e "s/node2_external/$node2_ip/g" inventory/mycluster/hosts.ini
sed -i -e "s/node2_internal/$node2_internal/g" inventory/mycluster/hosts.ini

ansible-playbook -i inventory/mycluster/hosts.ini cluster.yml

echo "Creating the kube config file in master........"
ssh -o "StrictHostKeyChecking no" core@$master_ip "mkdir -p ~/.kube; sudo cp /etc/kubernetes/admin.conf ~/.kube/config; sudo chmod 777 /home/core/.kube/config"

echo "Copying the yamls to master......."
scp ~/kubernetes/kube-terraform/templates/ClusterRoleBinding.yaml core@$master_ip:~/
scp ~/kubernetes/kube-terraform/templates/open-api.yaml core@$master_ip:~/
scp ~/kubernetes/kube-terraform/templates/dashboard-privs.yaml core@$master_ip:~/

#Execute the role binding yamls with kubectl
echo "Creating the service account and  cluster role bindings for Kubernetes......."
ssh -o "StrictHostKeyChecking no" core@$master_ip "kubectl create serviceaccount api-service-account"
ssh -o "StrictHostKeyChecking no" core@$master_ip "kubectl create -f ~/ClusterRoleBinding.yaml"
ssh -o "StrictHostKeyChecking no" core@$master_ip "kubectl create -f ~/open-api.yaml"
ssh -o "StrictHostKeyChecking no" core@$master_ip "kubectl create -f ~/dashboard-privs.yaml"

#Get the token from the master
ssh -o "StrictHostKeyChecking no"  core@$master_ip "sudo apt --assume-yes  install jq"
secret_name=`ssh -o "StrictHostKeyChecking no"  core@$master_ip "kubectl get serviceaccount api-service-account  -o json | jq -Mr '.secrets[].name'"`
token=`ssh -o "StrictHostKeyChecking no"  core@$master_ip "kubectl get secrets $secret_name -o json | jq -Mr '.data.token' | base64 -d"`

#Copy kube config to local
echo "Creating the kube config file in local......."
mkdir -p ~/.kube
cp ~/kubernetes/kube-terraform/templates/config ~/.kube/
sed -i -e "s/[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}/$master_ip/g" ~/.kube/config

sed -ri "s/^(\s*)(token\s*:\s*.*)/\1token: $token/" ~/.kube/config

echo "Installing the kubectl in local machine......"
curl -s -O https://storage.googleapis.com/kubernetes-release/release/v1.8.4/bin/linux/amd64/kubectl
chmod +x kubectl
sudo mv kubectl /usr/local/bin/kubectl

echo "*********Kubernetes Cluster creation is successful**********"

echo "************************************************************"
echo "****************Kubernetes Dashboard Details ***************"
echo "************************************************************"

echo "Kubernetes dashboard URL: https://$master_ip:6443/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login"
echo "Token : $token"

